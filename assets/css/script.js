document.addEventListener("DOMContentLoaded", function() {

function deletSlesh(){
  let base = document.querySelector('base').getAttribute('href');

  let baseSlesh = base.substring(base.length - 1);

  if( baseSlesh === '/'){
    let baseDel = base.replace(/.$/g, '');

    document.querySelector('base').href = baseDel;
  }
}
deletSlesh();

function addClassActive (){
	let about_title =  document.querySelector('.about_title'),
	menu_item = document.getElementsByClassName('menu_item'),
	contacts_title =  document.querySelector('.contacts_title');

	if( about_title ){
		menu_item[1].classList.toggle("active");
	}else if( contacts_title ){
		menu_item[2].classList.toggle("active");
	}else{
		menu_item[0].classList.toggle("active");
	}
}
addClassActive ();

var mySwiper = new Swiper ('.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
	loop: false,
	autoplay: {
		delay: 3000,
		disableOnInteraction: false,
	  },
    pagination: {
	  el: '.fanews_full_interface_list',
	  clickable: true,
	  
    },

  })

});




